//
//  main.m
//  BreastScan
//
//  Created by Mohit Aggarwal on 10/08/18.
//  Copyright © 2018 Mohit Aggarwal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BSAppDelegate.h"

int main(int argc, char * argv[]) {
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([BSAppDelegate class]));
	}
}
