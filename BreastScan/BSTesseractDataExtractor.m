//
//  ViewController.m
//  BreastScan
//
//  Created by Mohit Aggarwal on 10/08/18.
//  Copyright © 2018 Mohit Aggarwal. All rights reserved.
//

#import "BSTesseractDataExtractor.h"

#import <TesseractOCR/TesseractOCR.h>


@interface Tesseract : NSObject <G8TesseractDelegate>

@property (nonatomic, strong) NSOperationQueue *operationQueue;

@end

@implementation Tesseract

- (id)init {
	self = [super init];
	self.operationQueue = [[NSOperationQueue alloc] init];
	return self;
}

-(void)recognizeImageWithTesseract:(UIImage *)image withCompletion:(void (^)(NSString*))completionHandler {
	G8RecognitionOperation *operation = [[G8RecognitionOperation alloc] initWithLanguage:@"eng"];
	operation.tesseract.engineMode = G8OCREngineModeTesseractOnly;
	operation.tesseract.pageSegmentationMode = G8PageSegmentationModeAutoOnly;
	operation.delegate = self;
	operation.tesseract.image = image;
	operation.recognitionCompleteBlock = ^(G8Tesseract *tesseract) {
		// Fetch the recognized text
		NSString *recognizedText = tesseract.recognizedText;
		
		NSLog(@"%@", recognizedText);
		completionHandler(recognizedText);
	};
	
	// Finally, add the recognition operation to the queue
	[self.operationQueue addOperation:operation];
}

@end

@interface BSTesseractDataExtractor ()

@end

@implementation BSTesseractDataExtractor

+ (void)extractDataFromImage:(UIImage *)image withCompletion:(void (^)(NSString*))completionHandler {
	Tesseract *tesseract = [[Tesseract alloc] init];
	[tesseract recognizeImageWithTesseract:image withCompletion:completionHandler];
}

@end
