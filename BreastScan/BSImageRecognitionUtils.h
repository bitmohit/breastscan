//
//  ViewController.h
//  BreastScan
//
//  Created by Mohit Aggarwal on 10/08/18.
//  Copyright © 2018 Mohit Aggarwal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BSImageRecognitionUtils : NSObject

+ (NSDate *)getDobFromText:(NSString *)text;
+ (NSString *)getNameFromText:(NSString *)text;

@end

