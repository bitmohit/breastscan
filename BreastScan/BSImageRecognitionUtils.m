//
//  ViewController.m
//  BreastScan
//
//  Created by Mohit Aggarwal on 10/08/18.
//  Copyright © 2018 Mohit Aggarwal. All rights reserved.
//

#import "BSImageRecognitionUtils.h"

@implementation BSImageRecognitionUtils

+ (NSDate *)getDateFromLine:(NSString *)line {
	__block NSDate *date = nil;
	// Assuming date format as dd/mm/yyyy;
	NSError *error = NULL;
	NSRegularExpression *regex = [NSRegularExpression
								  regularExpressionWithPattern:@"([0-9]{1,2}?)[/-]+([0-9]{1,2}?)[/-]+([0-9]{4}?)"
								  options:NSRegularExpressionCaseInsensitive
								  error:&error];
	
	[regex enumerateMatchesInString:line options:0 range:NSMakeRange(0, [line length])
						 usingBlock:^(NSTextCheckingResult *match, NSMatchingFlags flags, BOOL *stop) {
							 NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
							 numberFormatter.numberStyle = NSNumberFormatterDecimalStyle;
							 
							 NSNumber *day = [numberFormatter numberFromString:[line substringWithRange:[match rangeAtIndex:1]]];
							 NSNumber *month = [numberFormatter numberFromString:[line substringWithRange:[match rangeAtIndex:2]]];
							 NSNumber *year = [numberFormatter numberFromString:[line substringWithRange:[match rangeAtIndex:3]]];
							 
							 NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
							 NSDateComponents *components = [[NSDateComponents alloc] init];
							 [components setDay:[day intValue]];
							 [components setMonth:[month intValue]];
							 [components setYear:[year intValue]];
							 date = [gregorianCalendar dateFromComponents:components];
							 NSLog(@"%@", date);
						 }
	 ];
	return date;
}

+ (NSDate *)getDobFromText:(NSString *)text {
	NSArray *dobWords = [[NSArray alloc] initWithObjects:@"dob", @"birth", nil];
	NSArray* lines = [text componentsSeparatedByCharactersInSet: [NSCharacterSet newlineCharacterSet]];
	NSMutableArray *dobLines = [[NSMutableArray alloc] init];
	for (int i=0;i<[lines count]; i++) {
		for (int j=0;j<[dobWords count]; j++) {
			if ([lines[i] rangeOfString:dobWords[j] options:NSCaseInsensitiveSearch].location != NSNotFound) {
				[dobLines addObject:lines[i]];
				break;
			}
		}
	}
	NSMutableArray *datesFound = [[NSMutableArray alloc] init];
	for (int i=0;i<[dobLines count];i++) {
		NSDate *date = [BSImageRecognitionUtils getDateFromLine:dobLines[i]];
		if (date != nil) {
			[datesFound addObject:date];
		}
	}
	NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"beginDate" ascending:TRUE];
	[datesFound sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
	return (datesFound == nil || [datesFound count] == 0) ? nil : datesFound[0];
}

+ (NSString *)getNameFromText:(NSString *)text {
	NSArray *nameWords = [[NSArray alloc] initWithObjects:@"name", nil];
	NSArray* lines = [text componentsSeparatedByCharactersInSet: [NSCharacterSet newlineCharacterSet]];
	NSMutableArray *nameLines = [[NSMutableArray alloc] init];
	for (int i=0;i<[lines count]; i++) {
		for (int j=0;j<[nameWords count]; j++) {
			if ([lines[i] rangeOfString:nameWords[j] options:NSCaseInsensitiveSearch].location != NSNotFound) {
				[nameLines addObject:lines[i]];
				NSLog(@"%@", lines[i]);
				break;
			}
		}
	}
	
	NSMutableArray *names = [[NSMutableArray alloc] init];
	for (int i=0;i<[nameLines count];i++) {
		NSString *line = nameLines[i];
		NSError *error = NULL;
		NSRegularExpression *regex = [NSRegularExpression
									  regularExpressionWithPattern:@".*name[: ]*([a-z ]+).*"
									  options:NSRegularExpressionCaseInsensitive
									  error:&error];
		[regex enumerateMatchesInString:line options:0 range:NSMakeRange(0, [line length])
							 usingBlock:^(NSTextCheckingResult *match, NSMatchingFlags flags, BOOL *stop) {
								 NSString *name = [line substringWithRange:[match rangeAtIndex:1]];
								 [names addObject:name];
							 }];
	}
	return ([names count] > 0) ? names[0] : nil;
}

@end
