//
//  ViewController.m
//  BreastScan
//
//  Created by Mohit Aggarwal on 10/08/18.
//  Copyright © 2018 Mohit Aggarwal. All rights reserved.
//

#import "BSImageRecognitionUtils.h"
#import "BSPatientDetailInputViewController.h"
#import "BSSubmitViewController.h"
#import "BSTesseractDataExtractor.h"
#import <MaterialButtons.h>
#import <MaterialButtons+ButtonThemer.h>
#import <MaterialTextFields.h>
@import Photos;

static NSArray *genders;

@interface BSPatientDetailInputViewController () <UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *contentView;

@property (weak, nonatomic) IBOutlet MDCTextField *firstNameField;
@property (weak, nonatomic) IBOutlet MDCTextField *lastNameField;
@property (weak, nonatomic) IBOutlet MDCTextField *dobField;
@property (weak, nonatomic) IBOutlet MDCTextField *genderField;
@property (weak, nonatomic) IBOutlet MDCMultilineTextField *allergyField;
@property (weak, nonatomic) IBOutlet MDCMultilineTextField *metalField;
@property (weak, nonatomic) IBOutlet MDCButton *submitButton;
@property (weak, nonatomic) IBOutlet UIButton *addImageButton;

@property (strong, nonatomic) MDCTextInputControllerOutlined *firstNameFieldController;
@property (strong, nonatomic) MDCTextInputControllerOutlined *lastNameFieldController;
@property (strong, nonatomic) MDCTextInputControllerOutlined *dobFieldController;
@property (strong, nonatomic) MDCTextInputControllerOutlined *genderFieldController;
@property (strong, nonatomic) MDCTextInputControllerOutlinedTextArea *allergyFieldController;
@property (strong, nonatomic) MDCTextInputControllerOutlinedTextArea *metalFieldController;

@property (strong, nonatomic) UIDatePicker *dobPicker;
@property (strong, nonatomic) UIPickerView *genderPicker;

@end

@implementation BSPatientDetailInputViewController

- (void)viewDidLoad {
	[super viewDidLoad];
	
	[self setTitle:@"Add Details"];
	[[self navigationItem] setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelPatientDetailInput)]];
	
	[[self firstNameField] setDelegate:self];
	[self setFirstNameFieldController:[[MDCTextInputControllerOutlined alloc] initWithTextInput:[self firstNameField]]];
	[[self firstNameFieldController] setPlaceholderText:@"First Name *"];
	[[self firstNameFieldController] setHelperText:@"* Required"];
	
	UIButton *captureImageButton = [UIButton buttonWithType:UIButtonTypeCustom];
	captureImageButton.frame = CGRectMake(0, 0, 40, 40);
	[captureImageButton addTarget:self action:@selector(addImageButtonClicked) forControlEvents:UIControlEventTouchUpInside];
	UIImage *cameraImage = [UIImage imageNamed:@"Camera.png"];
	[captureImageButton setImage:cameraImage forState:UIControlStateNormal];
	[[self firstNameField] setLeadingView:captureImageButton];
	[[self firstNameField] setLeadingViewMode:UITextFieldViewModeAlways];
	
	[self setLastNameFieldController:[[MDCTextInputControllerOutlined alloc] initWithTextInput:[self lastNameField]]];
	[[self lastNameFieldController] setPlaceholderText:@"Last Name"];
	
	UIDatePicker *datePicker = [[UIDatePicker alloc] init];
	[datePicker setDatePickerMode:UIDatePickerModeDate];
	[datePicker setMaximumDate:[NSDate date]];
	[self setDobPicker:datePicker];
	[[self dobField] setInputView:datePicker];
	[[self dobField] setInputAccessoryView:[self getDobFieldAccessoryView]];
	[[self dobField] setDelegate:self];
	
	[self setDobFieldController:[[MDCTextInputControllerOutlined alloc] initWithTextInput:[self dobField]]];
	[[self dobFieldController] setPlaceholderText:@"Date of Birth"];
	
	genders = [NSArray arrayWithObjects: @"Female", @"Male", @"Other", nil];
	UIPickerView *genderPicker = [[UIPickerView alloc] init];
	[genderPicker setDataSource:self];
	[genderPicker setDelegate:self];
	[self setGenderPicker:genderPicker];
	[[self genderField] setInputView:genderPicker];
	[[self genderField] setInputAccessoryView:[self getGenderFieldAccessoryView]];
	[[self genderField] setDelegate:self];
	
	[self setGenderFieldController:[[MDCTextInputControllerOutlined alloc] initWithTextInput:[self genderField]]];
	[[self genderFieldController] setPlaceholderText:@"Gender"];
	
	[self setAllergyFieldController:[[MDCTextInputControllerOutlinedTextArea alloc] initWithTextInput:[self allergyField]]];
	[[self allergyFieldController] setPlaceholderText:@"Allergies"];
	[[self allergyField] setMinimumLines:4];
	
	[self setMetalFieldController:[[MDCTextInputControllerOutlinedTextArea alloc] initWithTextInput:[self metalField]]];
	[[self metalFieldController] setPlaceholderText:@"Metals in your body"];
	[[self metalFieldController] setHelperText:@"eg. pacemaker, implants, replaced hip, knee etc."];
	[[self metalField] setMinimumLines:4];
	
	UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
	[[self view] addGestureRecognizer:tapGesture];
	
	MDCButtonScheme *buttonScheme = [[MDCButtonScheme alloc] init];
	[MDCContainedButtonThemer applyScheme:buttonScheme toButton:[self submitButton]];
	
	[[self submitButton] addTarget:self action:@selector(submitPatientDetails:) forControlEvents:UIControlEventTouchDown];
	
	[[self scrollView] setContentSize:[[self contentView] bounds].size];
}

- (void)viewWillAppear:(BOOL)animated {
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(keyboardWillShow:)
												 name:UIKeyboardWillShowNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(keyboardWillHide:)
												 name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
	if ([self getCurrentFirstResponder] == nil)
		return;
	
	[[self scrollView] setContentOffset:[[self getCurrentFirstResponder] frame].origin animated:YES];
}

- (void)keyboardWillShow:(NSNotification*)notification  {
	CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
	UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height, 0.0);
	
	[[self scrollView] setContentInset:contentInsets];
	[[self scrollView] setScrollIndicatorInsets:contentInsets];
}

- (void)keyboardWillHide:(NSNotification*)notification  {
	UIEdgeInsets contentInsets = UIEdgeInsetsZero;
	
	[[self scrollView] setContentInset:contentInsets];
	[[self scrollView] setScrollIndicatorInsets:contentInsets];
}

-(UIView *)getDobFieldAccessoryView {
	UIToolbar *toolbar= [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, [[self view] frame].size.width, 44)];
	[toolbar setBarStyle:UIBarStyleDefault];
	UIBarButtonItem* cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStyleDone target:self action:@selector(cancelDobInput:)];
	UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
	UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(commitDobInput:)];
	
	[toolbar setItems:[NSArray arrayWithObjects:cancelButton, flexibleSpace, doneButton, nil]];
	return toolbar;
}

- (void)cancelDobInput:(__unused id)sender {
	[[self dobField] resignFirstResponder];
}

- (void)commitDobInput:(__unused id)sender {
	UIDatePicker *datePicker = [self dobPicker];
	NSDate *dob = [datePicker date];
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"MMM d, yyyy"];
	NSString *dateString = [dateFormatter stringFromDate:dob];
	[[self dobField] setText:dateString];
	[[self dobField] resignFirstResponder];
}

-(UIView *)getGenderFieldAccessoryView {
	UIToolbar *toolbar= [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, [[self view] frame].size.width, 44)];
	[toolbar setBarStyle:UIBarStyleDefault];
	UIBarButtonItem* cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStyleDone target:self action:@selector(cancelGenderInput:)];
	UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
	UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(commitGenderInput:)];
	
	[toolbar setItems:[NSArray arrayWithObjects:cancelButton, flexibleSpace, doneButton, nil]];
	return toolbar;
}

- (void)cancelGenderInput:(__unused id)sender {
	[[self genderField] resignFirstResponder];
}

- (void)commitGenderInput:(__unused id)sender {
	UIPickerView *genderPicker = [self genderPicker];
	
	NSInteger gender = [genderPicker selectedRowInComponent:0];
	[[self genderField] setText:genders[gender]];
	[[self genderField] resignFirstResponder];
}

- (UIView *)getCurrentFirstResponder {
	if ([[self firstNameField] isFirstResponder])
		return [self firstNameField];
	else if ([[self lastNameField] isFirstResponder])
		return [self lastNameField];
	else if ([[self dobField] isFirstResponder])
		return [self dobField];
	else if ([[self genderField] isFirstResponder])
		return [self genderField];
	else if ([[self allergyField] isFirstResponder])
		return [self allergyField];
	else if ([[self metalField] isFirstResponder])
		return [self metalField];
	
	return nil;
}

- (void)dismissKeyboard {
	[[self getCurrentFirstResponder] resignFirstResponder];
}

- (void)submitPatientDetails:(__unused id)sender {
	if ([self isNameFieldEmpty]) {
		[[self firstNameFieldController] setErrorText:@"First Name cannot be empty" errorAccessibilityValue:nil];
		[[self scrollView] setContentOffset:CGPointMake(0, [[self firstNameField] frame].origin.y) animated:YES];
		return;
	}
	
	NSLog(@"Submit Successful");
	NSString *submitResult = [self getSubmitResult];
	if (submitResult == nil || [submitResult length] == 0) {
		return;
	}
	
	BSSubmitViewController *submitViewController = [[BSSubmitViewController alloc] initWithSubmitString:submitResult];
	UINavigationController *navigationController = [self navigationController];
	[navigationController pushViewController:submitViewController animated:YES];
}

- (NSString *)getSubmitResult {
	NSDictionary *formDict = [[NSDictionary alloc] initWithObjectsAndKeys:
						 [[self firstNameField] text], @"fname",
						 [[self lastNameField] text], @"lname",
						 [[self dobField] text], @"dob",
						 [[self allergyField] text], @"allergy",
						 [[self metalField] text], @"metals",
						 nil];
	NSError *error;
	NSData *postdata = [NSJSONSerialization dataWithJSONObject:formDict options:0 error:&error];
	NSString *formData = @"";
	if (! postdata) {
		NSLog(@"%s: error: %@", __func__, error.localizedDescription);
		return nil;
	} else {
		formData = [[NSString alloc] initWithData:postdata encoding:NSUTF8StringEncoding];
	}
	
	NSString *title = @"Congratulations! A request to the server has been made with the following attributes\n";
	NSString *url = @"URL : api/v1/personalDetails\n";
	NSString *requestMethod = @"Request Method : POST\n";
	NSString *contentType = @"Content-Type : application/json\n";
	NSString *body = [NSString stringWithFormat: @"%@%@%@", @"Body : ", formData, @"\n"];
	NSString *submitResult = [NSString stringWithFormat: @"%@%@%@%@%@", title, url, requestMethod, contentType, body];
	return submitResult;
}

- (void)addImageButtonClicked {
	[PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
		switch (status) {
		case PHAuthorizationStatusDenied:
			break;
		case PHAuthorizationStatusAuthorized:
			break;
		default:
			break;
		}
	}];
	
	UIImagePickerController* imagePicker = [[UIImagePickerController alloc]init];
	imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
	imagePicker.delegate = self;
	[self presentViewController:imagePicker animated:YES completion:nil];
}

- (BOOL)isNameFieldEmpty {
	NSString *firstName = [[[self firstNameField] text]stringByTrimmingCharactersInSet:
						   [NSCharacterSet whitespaceAndNewlineCharacterSet]];
	return firstName == nil || [firstName length] == 0;
}

- (void)cancelPatientDetailInput {
	[[self navigationController] dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark UITextFieldDelegate

- (void)textFieldDidEndEditing:(UITextField *)textField {
	if (textField == [self firstNameField]) {
		if ([self isNameFieldEmpty]) {
			[[self firstNameFieldController] setErrorText:@"First Name cannot be empty" errorAccessibilityValue:nil];
		}
		else {
			[[self firstNameFieldController] setErrorText:nil errorAccessibilityValue:nil];
		}
	}
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
	if (textField == [self genderField] || textField == [self dobField]) {
		return false;
	}
	return true;
}

#pragma mark UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
	return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(__unused NSInteger)component {
	return [genders count];
}

#pragma mark UIPickerViewDelegate

- (nullable NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
	return [genders objectAtIndex:row];
}

#pragma mark UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
	UIImage *image = info[UIImagePickerControllerOriginalImage];
	[picker dismissViewControllerAnimated:YES completion:nil];
	void (^imageRecognitionCompletion)(NSString*) = ^(NSString* result) {
		NSLog(@"%@", result);
		NSArray* lines = [result componentsSeparatedByCharactersInSet: [NSCharacterSet newlineCharacterSet]];
		NSMutableArray *dobLines = [[NSMutableArray alloc] init];
		for (int i=0;i<[lines count]; i++) {
			if ([lines[i] rangeOfString:@"dob" options:NSCaseInsensitiveSearch].location != NSNotFound) {
				[dobLines addObject:lines[i]];
			}
		}
		NSDate *birthDate = [BSImageRecognitionUtils getDobFromText:result];
		
		NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
		[dateFormat setDateFormat:@"MMM d, yyyy"];
		[dateFormat setTimeZone:[NSTimeZone systemTimeZone]];
		NSLog(@"Birth Date identified: %@", birthDate);
		NSString *birthDateString = [dateFormat stringFromDate:birthDate];
		if (birthDateString != nil && [birthDateString length] > 0) {
			[[self dobField] setText:birthDateString];
		}
		
		NSString *name = [BSImageRecognitionUtils getNameFromText:result];
		if (name != nil && [name length] > 0) {
			NSArray *nameComponents = [name componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
			if ([nameComponents count] > 0 && [nameComponents[0] length] > 0) {
				[[self firstNameField] setText:nameComponents[0]];
			}
			
			if ([nameComponents count] > 1 && [nameComponents[[nameComponents count] - 1] length] > 0) {
				[[self firstNameField] setText:nameComponents[[nameComponents count] - 1]];
			}
		}
	};
	[BSTesseractDataExtractor extractDataFromImage:image withCompletion:imageRecognitionCompletion];
}

@end
