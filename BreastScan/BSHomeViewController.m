//
//  ViewController.m
//  BreastScan
//
//  Created by Mohit Aggarwal on 10/08/18.
//  Copyright © 2018 Mohit Aggarwal. All rights reserved.
//

#import "BSHomeViewController.h"
#import "BSPatientDetailInputViewController.h"
#import <MaterialButtons.h>
#import <MaterialButtons+ButtonThemer.h>

@interface BSHomeViewController ()

@property (weak, nonatomic) IBOutlet MDCButton *addDetailsButton;

@end

@implementation BSHomeViewController

- (void)viewDidLoad {
	[super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
	[[self addDetailsButton] addTarget:self action:@selector(addPersonaDetailsButtonClicked:) forControlEvents:UIControlEventTouchDown];
	
	MDCButtonScheme *buttonScheme = [[MDCButtonScheme alloc] init];
	[MDCContainedButtonThemer applyScheme:buttonScheme toButton:[self addDetailsButton]];
}


- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

- (void)addPersonaDetailsButtonClicked:(id)sender {
	BSPatientDetailInputViewController *inputViewController = [[BSPatientDetailInputViewController alloc] initWithNibName:@"BSPatientDetailInputView" bundle:nil];
	
	UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:inputViewController];
	[self presentViewController:navigationController animated:YES completion:nil];
}

@end
