//
//  ViewController.m
//  BreastScan
//
//  Created by Mohit Aggarwal on 10/08/18.
//  Copyright © 2018 Mohit Aggarwal. All rights reserved.
//

#import "BSSubmitViewController.h"

@interface BSSubmitViewController ()

@property (strong, nonatomic) NSString *submitString;
@property (strong, nonatomic) UILabel *submitLabel;

@end

@implementation BSSubmitViewController

- (id)initWithSubmitString:(NSString *)submitString {
	self = [super init];
	[self setSubmitString:submitString];
	return self;
}

- (void)viewDidLoad {
	[super viewDidLoad];
	
	[[self view] setBackgroundColor:[UIColor whiteColor]];
	
	UILabel *submitLabel = [[UILabel alloc] init];
	[submitLabel setText:[self submitString]];
	[submitLabel setNumberOfLines:0];
	[submitLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
	[submitLabel setTextAlignment:NSTextAlignmentCenter];
	[[self view] addSubview:submitLabel];
	[self setSubmitLabel:submitLabel];
	[self addConstraints];
	[submitLabel sizeToFit];
	
	UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneButtonPressed)];
	[[self navigationItem] setRightBarButtonItem:doneButton];
	[[self navigationItem] setHidesBackButton:YES];
}

- (void)addConstraints {
	NSMutableArray<NSLayoutConstraint *> *constraints = [NSMutableArray array];
	UIView *view = [self view];
	UILabel *label = [self submitLabel];

	[constraints addObject:[NSLayoutConstraint constraintWithItem:label
														attribute:NSLayoutAttributeTop
														relatedBy:NSLayoutRelationEqual
														   toItem:view
														attribute:NSLayoutAttributeTop
													   multiplier:1.0
														 constant:250]];
	
	[constraints addObject:[NSLayoutConstraint constraintWithItem:label
														attribute:NSLayoutAttributeLeading
														relatedBy:NSLayoutRelationEqual
														   toItem:view
														attribute:NSLayoutAttributeLeading
													   multiplier:1.0
														 constant:50]];
	
	[constraints addObject:[NSLayoutConstraint constraintWithItem:label
														attribute:NSLayoutAttributeTrailing
														relatedBy:NSLayoutRelationEqual
														   toItem:view
														attribute:NSLayoutAttributeTrailing
													   multiplier:1.0
														 constant:-50]];
	
	[NSLayoutConstraint activateConstraints:constraints];
}

- (void)doneButtonPressed {
	[[self navigationController] dismissViewControllerAnimated:YES completion:nil];
}

@end
